//
//  WPConfig.h
//  WechatPlugin
//
//  Created by Stan on 2022/5/12.
//

#import <Foundation/Foundation.h>

@interface WPConfig : NSObject

//Singleton
+ (instancetype)sharedConfig;

//CallKit
@property (assign, nonatomic) BOOL callkitEnable;
//revoke msg
@property (assign, nonatomic) BOOL revokeEnable;

@end
