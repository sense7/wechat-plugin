//
//  WPConfig.m
//  WechatPlugin
//
//  Created by Stan on 2022/5/12.
//

#import "WPConfig.h"
#import "WXh.h"

static NSString * const kCallkitEnablekey = @"WPCallkitEnable";
static NSString * const kRevokeEnablekey = @"WPRevokeEnable";

@interface WPConfig ()

@end

@implementation WPConfig

+ (instancetype)sharedConfig {
    static WPConfig *config = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        config = [WPConfig new];
    });
    return config;
}

- (instancetype)init {
    if (self = [super init]) {
        _callkitEnable = [[NSUserDefaults standardUserDefaults] boolForKey:kCallkitEnablekey];
        _revokeEnable = [[NSUserDefaults standardUserDefaults] boolForKey:kRevokeEnablekey];
    }
    
    return self;
}

- (void)setCallkitEnable:(BOOL)callkitEnable {
    _callkitEnable = callkitEnable;
    
    [[NSUserDefaults standardUserDefaults] setBool:callkitEnable forKey:kCallkitEnablekey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setRevokeEnable:(BOOL)revokeEnable {
    _revokeEnable = revokeEnable;
    
    [[NSUserDefaults standardUserDefaults] setBool:revokeEnable forKey:kRevokeEnablekey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
