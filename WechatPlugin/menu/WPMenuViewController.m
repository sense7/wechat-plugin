//
//  WPMenuViewController.m
//  WechatPlugin
//  菜单控制
//  Created by Stan on 2022/5/12.
//

#import <UIKit/UIKit.h>
#import "WXh.h"
#import "WPMenuViewController.h"
#import "WPConfig.h"
#import <objc/objc-runtime.h>

@interface WPMenuViewController () <MultiSelectContactsViewControllerDelegate>

@property (nonatomic, strong) WCTableViewManager *tableViewMgr;

@end

@implementation WPMenuViewController
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        _tableViewMgr = [[objc_getClass("WCTableViewManager") alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStyleGrouped];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTitle];
    [self reloadTableData];
    self.edgesForExtendedLayout = UIRectEdgeNone;

    MMTableView *tableView = [self.tableViewMgr getTableView];
    [self.view addSubview:tableView];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [self stopLoading];
}

- (void)initTitle {
    self.title = @"微信助手🐶";
}

- (void)reloadTableData {
    [self.tableViewMgr clearAllSection];

    [self addBasicSettingSection];

    [self addAboutSection];

    MMTableView *tableView = [self.tableViewMgr getTableView];
    [tableView reloadData];
}

- (void)addBasicSettingSection {
    WCTableViewSectionManager *sectionInfo = [objc_getClass("WCTableViewSectionManager") sectionInfoHeader:@"基本功能"];

    [sectionInfo addCell:[self createAbortRevokeMessageCell]];
    [sectionInfo addCell:[self createCallkitCell]];


    [self.tableViewMgr addSection:sectionInfo];
}

#pragma mark - function toggle
- (WCTableViewSectionManager *)createAbortRevokeMessageCell {
    return [objc_getClass("WCTableViewCellManager") switchCellForSel:@selector(settingMessageRevoke:) target:self title:@"消息防撤回" on:[WPConfig sharedConfig].revokeEnable];
}

- (void)settingMessageRevoke:(UISwitch *)revokeSwitch {
    [WPConfig sharedConfig].revokeEnable = revokeSwitch.on;
}

- (WCTableViewSectionManager *)createCallkitCell {
    return [objc_getClass("WCTableViewCellManager") switchCellForSel:@selector(settingCallkit:) target:self title:@"Callkit" on:[WPConfig sharedConfig].callkitEnable];
}

- (void)settingCallkit:(UISwitch *)callkitSwitch {
    [WPConfig sharedConfig].callkitEnable = callkitSwitch.on;
}

#pragma mark - About
- (void)addAboutSection {
    WCTableViewSectionManager *sectionInfo = [objc_getClass("WCTableViewSectionManager") sectionInfoDefaut];

    [sectionInfo addCell:[self createGithubCell]];

    [self.tableViewMgr addSection:sectionInfo];
}

- (WCTableViewCellManager *)createGithubCell {
    return [objc_getClass("WCTableViewCellManager") normalCellForSel:@selector(showGithub) target:self title:@"项目 Github" rightValue: @"★ star" WithDisclosureIndicator:1];
}

- (void)showGithub {
    NSURL *gitHubUrl = [NSURL URLWithString:@"https://gitee.com/sense7/wechat-plugin"];
    MMWebViewController *webViewController = [[objc_getClass("MMWebViewController") alloc] initWithURL:gitHubUrl presentModal:NO extraInfo:nil];
    [self.navigationController PushViewController:webViewController animated:YES];
}

@end
