//
//  WPBaseViewController.h
//  WechatPlugin
//
//  Created by Stan on 2022/5/12.
//

#import <UIKit/UIKit.h>

@interface WPBaseViewController : UIViewController

- (void)startLoadingBlocked;
- (void)startLoadingNonBlock;
- (void)startLoadingWithText:(NSString *)text;
- (void)stopLoading;
- (void)stopLoadingWithFailText:(NSString *)text;
- (void)stopLoadingWithOKText:(NSString *)text;

@end

